import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import moment from 'moment';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit {
  stockPickerForm: FormGroup;
  maxDate: Date = new Date();

  quotes$ = this.priceQuery.priceQueries$;

  constructor(private fb: FormBuilder, private priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required]
    });
  }

  ngOnInit() {
    // This for first task
   // this.stockPickerForm.valueChanges.subscribe(this.fetchQuote.bind(this));
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, startDate, endDate } = this.stockPickerForm.value;
      const period = this.getPeriodFromStartDate(startDate);
      this.priceQuery.fetchQuote(symbol, period, startDate, endDate);
    }
  }

  getPeriodFromStartDate(startDate: string) {
    const numOfDays: number = moment().diff(moment(startDate), 'days');
    switch (true) {
      case (numOfDays <= 30)://1 month
        return "1m";
      case (numOfDays <= 90)://2 month
        return "3m";
      case (numOfDays <= 180)://3 month
        return "6m";
      case (numOfDays <= 365): // 1 year
        return "1y";
      case (numOfDays <= 730)://2 month
        return "2y";
      case (numOfDays <= 1825)://5 month
        return "5y";
      default:
        return "max";
    }
  }

  changeStDate() {
    const startDate = this.stockPickerForm.controls['startDate'].value;
    const endDate = this.stockPickerForm.controls['endDate'].value;

    console.log('start date');
 
    if (startDate && endDate && (endDate < startDate)) {
      this.stockPickerForm.patchValue({endDate:  startDate});
    }
  }

  changeEndDate() {
    const startDate = this.stockPickerForm.controls['startDate'].value;
    const endDate = this.stockPickerForm.controls['endDate'].value;
    console.log('end date');

    if (startDate && endDate && (startDate > endDate)) {
      this.stockPickerForm.patchValue({startDate:  endDate});
    }
  }
}
