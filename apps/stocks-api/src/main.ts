/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
import { environment } from './environments/environment';
const NodeCache = require('node-cache');
const Request = require('request');
const stocksApiCache = new NodeCache();

const init = async () => {
  const server = new Server({
    port: 3333,
    host: 'localhost'
  });

  function getChartDetails(url: string) {
    console.log("entering server call");
    return new Promise((resolve, reject) => {
      Request(url, (error, res, body) => {
        if (error) reject(error);
        if (res) {
          console.log(res['statusCode']);
          if (res['statusCode'] !== 200)
            return res;
        }
        resolve(body);
      });
    });
  }

  server.route({
    method: 'GET',
    path: '/chart/{symbol}/{period}',
    handler: (request, h) => {
      const { symbol } = request.params;
      const { period } = request.params;
      const apiKey = environment.apiKey;
      const apiUrl = environment.apiURL + '/beta/stock/' + symbol + '/chart/' + period + '?token=' + apiKey;
      let response = null;
      //create a checksum
      let checkSum = symbol + period;
      stocksApiCache.get(checkSum, function (err, value) {
        if (!err) {
          if (value !== undefined) {
            console.log("Loading from cache");
            response = value;
          }
        }
      });
      if(response) return response;
        return getChartDetails(apiUrl).then(response => {
        stocksApiCache.set(checkSum, response);
        console.log("Loading from Actual Server");
        return response;
      });
    }    
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
